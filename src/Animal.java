/**
 * Created by General on 2/15/2017.
 */
public class Animal {
    public Animal(){
        age = Integer.valueOf(0);
        name = "unknown";
    }

    public Animal(Integer age, String name) {
        this.age = age;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static void main(String[] args) {
        // write your code here
    }

    private Integer age;
    private  String name;

}
